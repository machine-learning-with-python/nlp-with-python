def scorer(doc):
    """
    This function calculates a score based on the unique tags found in the given document.

    Parameters:
    doc (list): A list of tokens where each token has a 'tag_' attribute.

    Returns:
    str: A string containing the number of unique tags, the number of tokens used, the calculated score, and a congratulatory message.
    """

    # List of all possible tags
    tags = ['AFX', 'JJ', 'JJR', 'JJS', 'PDT', 'PRP$', 'WDT', 'WP$', 'IN', 'EX', 'RB', 'RBR', 'RBS', 'WRB', 'CC', 'DT', 'UH', 'NN', 'NNS', 'WP', 'CD', 'POS', 'RP', 'TO', 'PRP', 'NNP', 'NNPS', '-LRB-', '-RRB-', ',', ':', '.', "''", '""', '``', 'HYPH', 'LS', 'NFP', '_SP', '#', '$', 'SYM', 'BES', 'HVS', 'MD', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'ADD', 'FW', 'GW', 'XX', 'NIL']

    # Initialize counter for unique tags
    counter = 0

    # Loop through each tag
    for tag in tags:
        # Loop through each token in the document
        for token in doc:
            # If the token's tag matches the current tag, increment the counter and break the inner loop
            if token.tag_ == tag:
                counter+=1
                break

    # Calculate the score as the maximum of counter*3 - len(doc) and counter
    score = max(counter*3 - len(doc),counter)

    # Return a formatted string with the results
    return f'Unique tags: {counter}\nTokens used: {len(doc)}\nSCORE: {score}\nCONGRATULATIONS!'