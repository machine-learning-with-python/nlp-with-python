{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Regular Expressions\n",
    "\n",
    "Regular Expressions (sometimes called regex for short) allow a user to search for strings using almost any sort of rule they can come up with. For example, finding all capital letters in a string, or finding a phone number in a document. \n",
    "\n",
    "Regular expressions are notorious for their seemingly strange syntax. This strange syntax is a byproduct of their flexibility. Regular expressions have to be able to filter out any string pattern you can imagine, which is why they have a complex string pattern format.\n",
    "\n",
    "Regular expressions are handled using Python's built-in **re** library. See [the docs](https://docs.python.org/3/library/re.html) for more information."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's begin by explaining how to search for basic patterns in a string!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Searching for Basic Patterns\n",
    "\n",
    "Let's imagine that we have the following string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Raw strings are used so that backslashes do not have to be escaped.\n",
    "text = \"The agent's phone number is 408-555-1234. Call soon!\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll start off by trying to find out if the string \"phone\" is inside the text string. Now we could quickly do this with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Check if phone is in text\n",
    "'phone' in text"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But let's show the format for regular expressions, because later on we will be searching for patterns that won't have such a simple solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the regex module\n",
    "import re"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we can find patterns in text\n",
    "pattern = 'phone'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(12, 17), match='phone'>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# This returns a match object\n",
    "re.search(pattern,text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define pattern\n",
    "pattern = \"NOT IN TEXT\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This returns a None object\n",
    "re.search(pattern,text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we've seen that re.search() will take the pattern, scan the text, and then returns a Match object. If no pattern is found, a None is returned (in Jupyter Notebook this just means that nothing is output below the cell).\n",
    "\n",
    "Let's take a closer look at this Match object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define pattern\n",
    "pattern = 'phone'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This returns a Match object\n",
    "match = re.search(pattern,text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(12, 17), match='phone'>"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Show start of match\n",
    "match"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the span, there is also a start and end index information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(12, 17)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the span method on the match object\n",
    "# This method returns a tuple containing the start and end positions of the match\n",
    "match.span()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the start method on the match object\n",
    "# This method returns the start position of the match\n",
    "match.start()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "17"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the end method on the match object\n",
    "# This method returns the end position of the match\n",
    "match.end()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But what if the pattern occurs more than once?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a string variable 'text' with the content \"my phone is a new phone\"\n",
    "text = \"my phone is a new phone\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.search method to find the first occurrence of the string \"phone\" in the 'text' variable\n",
    "# The re.search method returns a Match object if the string is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "match = re.search(\"phone\",text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 8)"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the span method on the match object\n",
    "# This method returns a tuple containing the start and end positions of the match\n",
    "match.span()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice it only matches the first instance. If we wanted a list of all matches, we can use .findall() method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.findall method to find all occurrences of the string \"phone\" in the 'text' variable\n",
    "# The re.findall method returns a list of all matches of the pattern in the string\n",
    "# Each match in the list is a string that matches the pattern\n",
    "matches = re.findall(\"phone\",text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['phone', 'phone']"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# The 'matches' variable contains the list of all matches of the pattern in the string.\n",
    "# Each match in the list is a string that matches the pattern.\n",
    "matches"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(matches)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get actual match objects, use the iterator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(3, 8)\n",
      "(18, 23)\n"
     ]
    }
   ],
   "source": [
    "# Iterate over each match of the string \"phone\" in the 'text' variable\n",
    "# The re.finditer method returns an iterator yielding match objects for each match\n",
    "for match in re.finditer(\"phone\",text):\n",
    "    # For each match, print the span\n",
    "    # The span method on a match object returns a tuple containing the start and end positions of the match\n",
    "    print(match.span())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you wanted the actual text that matched, you can use the .group() method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'phone'"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the group method on the match object\n",
    "# This method returns the matched string\n",
    "match.group()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Patterns\n",
    "\n",
    "So far we've learned how to search for a basic string. What about more complex examples? Such as trying to find a telephone number in a large string of text? Or an email address?\n",
    "\n",
    "We could just use search method if we know the exact phone or email, but what if we don't know it? We may know the general format, and we can use that along with regular expressions to search the document for strings that match a particular pattern.\n",
    "\n",
    "This is where the syntax may appear strange at first, but take your time with this; often it's just a matter of looking up the pattern code.\n",
    "\n",
    "Let's begin!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Identifiers for Characters in Patterns\n",
    "\n",
    "Characters such as a digit or a single string have different codes that represent them. You can use these to build up a pattern string. Notice how these make heavy use of the backwards slash \\ . Because of this when defining a pattern string for regular expression we use the format:\n",
    "\n",
    "    r'mypattern'\n",
    "    \n",
    "placing the r in front of the string allows python to understand that the \\ in the pattern string are not meant to be escape slashes.\n",
    "\n",
    "Below you can find a table of all the possible identifiers:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table ><tr><th>Character</th><th>Description</th><th>Example Pattern Code</th><th >Exammple Match</th></tr>\n",
    "\n",
    "<tr ><td><span >\\d</span></td><td>A digit</td><td>file_\\d\\d</td><td>file_25</td></tr>\n",
    "\n",
    "<tr ><td><span >\\w</span></td><td>Alphanumeric</td><td>\\w-\\w\\w\\w</td><td>A-b_1</td></tr>\n",
    "\n",
    "\n",
    "\n",
    "<tr ><td><span >\\s</span></td><td>White space</td><td>a\\sb\\sc</td><td>a b c</td></tr>\n",
    "\n",
    "\n",
    "\n",
    "<tr ><td><span >\\D</span></td><td>A non digit</td><td>\\D\\D\\D</td><td>ABC</td></tr>\n",
    "\n",
    "<tr ><td><span >\\W</span></td><td>Non-alphanumeric</td><td>\\W\\W\\W\\W\\W</td><td>*-+=)</td></tr>\n",
    "\n",
    "<tr ><td><span >\\S</span></td><td>Non-whitespace</td><td>\\S\\S\\S\\S</td><td>Yoyo</td></tr></table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a string variable 'text' with the content \"My telephone number is 408-555-1234\"\n",
    "text = \"My telephone number is 408-555-1234\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.search method to find the first occurrence of a phone number pattern in the 'text' variable\n",
    "# The phone number pattern is defined as three digits followed by a hyphen, followed by three more digits, another hyphen, and finally four digits\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "phone = re.search(r'\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d',text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'408-555-1234'"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Call the group method on the match object\n",
    "# This method returns the matched string\n",
    "phone.group()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the repetition of \\d. That is a bit of an annoyance, especially if we are looking for very long strings of numbers. Let's explore the possible quantifiers.\n",
    "\n",
    "## Quantifiers\n",
    "\n",
    "Now that we know the special character designations, we can use them along with quantifiers to define how many we expect."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table ><tr><th>Character</th><th>Description</th><th>Example Pattern Code</th><th >Exammple Match</th></tr>\n",
    "\n",
    "<tr ><td><span >+</span></td><td>Occurs one or more times</td><td>\tVersion \\w-\\w+</td><td>Version A-b1_1</td></tr>\n",
    "\n",
    "<tr ><td><span >{3}</span></td><td>Occurs exactly 3 times</td><td>\\D{3}</td><td>abc</td></tr>\n",
    "\n",
    "\n",
    "\n",
    "<tr ><td><span >{2,4}</span></td><td>Occurs 2 to 4 times</td><td>\\d{2,4}</td><td>123</td></tr>\n",
    "\n",
    "\n",
    "\n",
    "<tr ><td><span >{3,}</span></td><td>Occurs 3 or more</td><td>\\w{3,}</td><td>anycharacters</td></tr>\n",
    "\n",
    "<tr ><td><span >\\*</span></td><td>Occurs zero or more times</td><td>A\\*B\\*C*</td><td>AAACC</td></tr>\n",
    "\n",
    "<tr ><td><span >?</span></td><td>Once or none</td><td>plurals?</td><td>plural</td></tr></table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's rewrite our pattern using these quantifiers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(23, 35), match='408-555-1234'>"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.search method to find the first occurrence of a phone number pattern in the 'text' variable\n",
    "# The phone number pattern is defined as three digits followed by a hyphen, followed by three more digits, another hyphen, and finally four digits\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "re.search(r'\\d{3}-\\d{3}-\\d{4}',text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Groups\n",
    "\n",
    "What if we wanted to do two tasks, find phone numbers, but also be able to quickly extract their area code (the first three digits). We can use groups for any general task that involves grouping together regular expressions (so that we can later break them down). \n",
    "\n",
    "Using the phone number example, we can separate groups of regular expressions using parentheses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a pattern for a phone number using the re.compile function\n",
    "# The pattern is defined as three digits (\\d{3}), followed by a hyphen, followed by three more digits (\\d{3}), another hyphen, and finally four digits (\\d{4})\n",
    "# Each group of digits is enclosed in parentheses to create groups, which can be accessed individually later on\n",
    "# The re.compile function compiles a regular expression pattern into a regular expression object, which can be used for matching using its match() and search() methods\n",
    "phone_pattern = re.compile(r'(\\d{3})-(\\d{3})-(\\d{4})')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.search method to find the first occurrence of the phone number pattern in the 'text' variable\n",
    "# The phone number pattern is defined by the 'phone_pattern' variable\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "results = re.search(phone_pattern,text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'408-555-1234'"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# The entire result\n",
    "results.group()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'408'"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Can then also call by group position.\n",
    "# remember groups were separated by parentheses ()\n",
    "# Something to note is that group ordering starts at 1. Passing in 0 returns everything\n",
    "results.group(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'555'"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results.group(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1234'"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results.group(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "ename": "IndexError",
     "evalue": "no such group",
     "output_type": "error",
     "traceback": [
      "\u001B[1;31m---------------------------------------------------------------------------\u001B[0m",
      "\u001B[1;31mIndexError\u001B[0m                                Traceback (most recent call last)",
      "\u001B[1;32m<ipython-input-32-79a918a9b5dc>\u001B[0m in \u001B[0;36m<module>\u001B[1;34m()\u001B[0m\n\u001B[0;32m      1\u001B[0m \u001B[1;31m# We only had three groups of parentheses\u001B[0m\u001B[1;33m\u001B[0m\u001B[1;33m\u001B[0m\u001B[0m\n\u001B[1;32m----> 2\u001B[1;33m \u001B[0mresults\u001B[0m\u001B[1;33m.\u001B[0m\u001B[0mgroup\u001B[0m\u001B[1;33m(\u001B[0m\u001B[1;36m4\u001B[0m\u001B[1;33m)\u001B[0m\u001B[1;33m\u001B[0m\u001B[0m\n\u001B[0m",
      "\u001B[1;31mIndexError\u001B[0m: no such group"
     ]
    }
   ],
   "source": [
    "# We only had three groups of parentheses\n",
    "results.group(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Regex Syntax\n",
    "\n",
    "### Or operator |\n",
    "\n",
    "Use the pipe operator to have an **or** statment. For example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(5, 8), match='man'>"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.search method to find the first occurrence of either \"man\" or \"woman\" in the string \"This man was here.\"\n",
    "# The \"|\" operator acts as a logical OR, matching either \"man\" or \"woman\".\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not.\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself.\n",
    "re.search(r\"man|woman\",\"This man was here.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(5, 10), match='woman'>"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.search method to find the first occurrence of either \"man\" or \"woman\" in the string \"This woman was here.\"\n",
    "# The \"|\" operator acts as a logical OR, matching either \"man\" or \"woman\".\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not.\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself.\n",
    "re.search(r\"man|woman\",\"This woman was here.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Wildcard Character\n",
    "\n",
    "Use a \"wildcard\" as a placement that will match any character placed there. You can use a simple period **.** for this. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['cat', 'hat', 'sat']"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of the pattern \".at\" in the string \"The cat in the hat sat here.\"\n",
    "# The pattern \".at\" is a regular expression that matches any three-character string ending with \"at\".\n",
    "# The \".\" is a wildcard that matches any character, and \"at\" matches the exact string \"at\".\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern.\n",
    "re.findall(r\".at\",\"The cat in the hat sat here.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['bat', 'lat']"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of the pattern \".at\" in the string \"The bat went splat\"\n",
    "# The pattern \".at\" is a regular expression that matches any three-character string ending with \"at\".\n",
    "# The \".\" is a wildcard that matches any character, and \"at\" matches the exact string \"at\".\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern.\n",
    "re.findall(r\".at\",\"The bat went splat\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how we only matched the first 3 letters, that is because we need a **.** for each wildcard letter. Or use the quantifiers described above to set its own rules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['e bat', 'splat']"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of the pattern \"...at\" in the string \"The bat went splat\"\n",
    "# The pattern \"...at\" is a regular expression that matches any four-character string ending with \"at\".\n",
    "# The \".\" is a wildcard that matches any character, and \"at\" matches the exact string \"at\".\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern.\n",
    "re.findall(r\"...at\",\"The bat went splat\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However this still leads the problem to grabbing more beforehand. Really we only want words that end with \"at\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['bat', 'splat']"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of the pattern \"\\S+at\" in the string \"The bat went splat\"\n",
    "# The pattern \"\\S+at\" is a regular expression that matches any non-whitespace characters (\\S+) ending with \"at\".\n",
    "# The \"+\" is a quantifier that matches one or more of the preceding element.\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern.\n",
    "re.findall(r'\\S+at',\"The bat went splat\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Starts With and Ends With\n",
    "\n",
    "We can use the **^** to signal starts with, and the **$** to signal ends with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['2']"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of a pattern in a string\n",
    "# The pattern '\\d$' is a regular expression that matches any string ending with a digit\n",
    "# The '\\d' is a special character that matches any digit, and the '$' is a special character that matches the end of a string\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall(r'\\d$', 'This ends with a number 2')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['1']"
      ]
     },
     "execution_count": 40,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all occurrences of a pattern in a string\n",
    "# The pattern '^\\d' is a regular expression that matches any string starting with a digit\n",
    "# The '^' is a special character that matches the start of a string, and '\\d' is a special character that matches any digit\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall(r'^\\d','1 is the loneliest number.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this is for the entire string, not individual words!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exclusion\n",
    "\n",
    "To exclude characters, we can use the **^** symbol in conjunction with a set of brackets **[]**. Anything inside the brackets is excluded. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a string variable 'phrase' with the content \"there are 3 numbers 34 inside 5 this sentence.\"\n",
    "phrase = \"there are 3 numbers 34 inside 5 this sentence.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['t',\n",
       " 'h',\n",
       " 'e',\n",
       " 'r',\n",
       " 'e',\n",
       " ' ',\n",
       " 'a',\n",
       " 'r',\n",
       " 'e',\n",
       " ' ',\n",
       " ' ',\n",
       " 'n',\n",
       " 'u',\n",
       " 'm',\n",
       " 'b',\n",
       " 'e',\n",
       " 'r',\n",
       " 's',\n",
       " ' ',\n",
       " ' ',\n",
       " 'i',\n",
       " 'n',\n",
       " 's',\n",
       " 'i',\n",
       " 'd',\n",
       " 'e',\n",
       " ' ',\n",
       " ' ',\n",
       " 't',\n",
       " 'h',\n",
       " 'i',\n",
       " 's',\n",
       " ' ',\n",
       " 's',\n",
       " 'e',\n",
       " 'n',\n",
       " 't',\n",
       " 'e',\n",
       " 'n',\n",
       " 'c',\n",
       " 'e',\n",
       " '.']"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all non-digit characters in the 'phrase' variable\n",
    "# The pattern '[^\\d]' is a regular expression that matches any character that is not a digit\n",
    "# The '^' inside the brackets is a special character that inverts the character set, making it match any character that is not in the set\n",
    "# The '\\d' is a special character that matches any digit\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall(r'[^\\d]',phrase)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get the words back together, use a + sign "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['there are ', ' numbers ', ' inside ', ' this sentence.']"
      ]
     },
     "execution_count": 43,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all non-digit sequences in the 'phrase' variable\n",
    "# The pattern '[^\\d]+' is a regular expression that matches any sequence of characters that are not digits\n",
    "# The '^' inside the brackets is a special character that inverts the character set, making it match any character that is not in the set\n",
    "# The '\\d' is a special character that matches any digit\n",
    "# The '+' is a quantifier that matches one or more of the preceding element\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall(r'[^\\d]+',phrase)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this to remove punctuation from a sentence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_phrase = 'This is a string! But it has punctuation. How can we remove it?'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['This',\n",
       " 'is',\n",
       " 'a',\n",
       " 'string',\n",
       " 'But',\n",
       " 'it',\n",
       " 'has',\n",
       " 'punctuation',\n",
       " 'How',\n",
       " 'can',\n",
       " 'we',\n",
       " 'remove',\n",
       " 'it']"
      ]
     },
     "execution_count": 45,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all sequences of characters in the 'test_phrase' variable that are not punctuation or spaces\n",
    "# The pattern '[^!.? ]+' is a regular expression that matches any sequence of characters that are not punctuation (., !, ?) or spaces\n",
    "# The '^' inside the brackets is a special character that inverts the character set, making it match any character that is not in the set\n",
    "# The '+' is a quantifier that matches one or more of the preceding element\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall('[^!.? ]+',test_phrase)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.findall method to find all sequences of characters in the 'test_phrase' variable that are not punctuation or spaces\n",
    "# The pattern '[^!.? ]+' is a regular expression that matches any sequence of characters that are not punctuation (., !, ?) or spaces\n",
    "# The '^' inside the brackets is a special character that inverts the character set, making it match any character that is not in the set\n",
    "# The '+' is a quantifier that matches one or more of the preceding element\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "# The ' '.join() method is used to join the list of substrings into a single string with spaces in between\n",
    "clean = ' '.join(re.findall('[^!.? ]+',test_phrase))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'This is a string But it has punctuation How can we remove it'"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "clean"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Brackets for Grouping\n",
    "\n",
    "As we showed above we can use brackets to group together options, for example if we wanted to find hyphenated words:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [],
   "source": [
    "text = 'Only find the hypen-words in this sentence. But you do not know how long-ish they are'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['hypen-words', 'long-ish']"
      ]
     },
     "execution_count": 49,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.findall method to find all hyphenated words in the 'text' variable\n",
    "# The pattern '[\\w]+-[\\w]+' is a regular expression that matches any word character (alphanumeric or underscore) followed by a hyphen, and then followed by any word character\n",
    "# The '+' is a quantifier that matches one or more of the preceding element\n",
    "# The re.findall method returns a list of all substrings in the given string that match the pattern\n",
    "re.findall(r'[\\w]+-[\\w]+',text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parentheses for Multiple Options\n",
    "\n",
    "If we have multiple options for matching, we can use parentheses to list out these options. For Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find words that start with cat and end with one of these options: 'fish','nap', or 'claw'\n",
    "text = 'Hello, would you like some catfish?'\n",
    "texttwo = \"Hello, would you like to take a catnap?\"\n",
    "textthree = \"Hello, have you seen this caterpillar?\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(27, 34), match='catfish'>"
      ]
     },
     "execution_count": 51,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.search method to find the first occurrence of a pattern in the 'text' variable\n",
    "# The pattern 'cat(fish|nap|claw)' is a regular expression that matches the string \"cat\" followed by either \"fish\", \"nap\", or \"claw\"\n",
    "# The parentheses enclose the options, and the \"|\" operator acts as a logical OR, matching either \"fish\", \"nap\", or \"claw\"\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "re.search(r'cat(fish|nap|claw)',text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<_sre.SRE_Match object; span=(32, 38), match='catnap'>"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use the re.search method to find the first occurrence of a pattern in the 'texttwo' variable\n",
    "# The pattern 'cat(fish|nap|claw)' is a regular expression that matches the string \"cat\" followed by either \"fish\", \"nap\", or \"claw\"\n",
    "# The parentheses enclose the options, and the \"|\" operator acts as a logical OR, matching either \"fish\", \"nap\", or \"claw\"\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "re.search(r'cat(fish|nap|claw)',texttwo)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the re.search method to find the first occurrence of a pattern in the 'textthree' variable\n",
    "# The pattern 'cat(fish|nap|claw)' is a regular expression that matches the string \"cat\" followed by either \"fish\", \"nap\", or \"claw\"\n",
    "# The parentheses enclose the options, and the \"|\" operator acts as a logical OR, matching either \"fish\", \"nap\", or \"claw\"\n",
    "# The re.search method returns a Match object if the pattern is found, or None if it's not\n",
    "# The Match object contains information about the match, including the span of the match and the matched string itself\n",
    "# In this case, as the pattern is not found in 'textthree', None is returned\n",
    "re.search(r'cat(fish|nap|claw)',textthree)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conclusion\n",
    "\n",
    "Excellent work! For full information on all possible patterns, check out: https://docs.python.org/3/howto/regex.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Next up: Python Text Basics Assessment"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
